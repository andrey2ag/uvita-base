package com.uvita.myapp.general;


import androidx.multidex.MultiDexApplication;

import com.facebook.stetho.Stetho;
import com.uvita.myapp.BuildConfig;
import com.uvita.myapp.modules.repository.local.AppDB;


public class MyApplication extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics());
//        ViewPump.init(ViewPump.builder()
//                .addInterceptor(new CalligraphyInterceptor(
//                        new CalligraphyConfig.Builder()
//                                .setDefaultFontPath("fonts/roboto/Roboto-Light.ttf")
//                                .setFontAttrId(R.attr.fontPath)
//                                .build()))
//                .build());
        // Initialize DB INSTANCE
        AppDB.createAppDatabase(this);

        checkAndInitStetho();
    }

    private void checkAndInitStetho() {
        //Stetho integration
        if (BuildConfig.DEBUG) {
            try {
//                SyncTask.executeAndWait(() ->
                Stetho.initialize(
                        Stetho.newInitializerBuilder(MyApplication.this)
                                .enableDumpapp(Stetho.defaultDumperPluginsProvider(MyApplication.this))
                                .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(MyApplication.this))
                                .build());
//            );
            } catch (Exception e) {
//                LOGE(LOG_TAG, "Error on checkAndInitStetho()", e);
            }
        }
    }

}
