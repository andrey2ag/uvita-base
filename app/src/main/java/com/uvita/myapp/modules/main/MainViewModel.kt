package com.uvita.myapp.modules.main

import com.uvita.myapp.general.MyApp
import com.uvita.myapp.modules.baseComponents.BaseViewModel
import com.uvita.myapp.modules.repository.remote.inspections.InspectionsRepository
import org.greenrobot.eventbus.Subscribe

class MainViewModel : BaseViewModel<MainView>() {

    init {
        MyApp.getEventBus().register(this)
        val inspectionsRepository = InspectionsRepository()
//        inspectionsRepository.getProjects()
    }

    override fun onStart() {
        if (!MyApp.getEventBus().isRegistered(this)) {
            MyApp.getEventBus().register(this)
        }
    }

    override fun onStop() {
        MyApp.getEventBus().unregister(this)
    }

    @Subscribe
    fun onSetTitleEvent(event: SetTitleEvent) {
        if (view != null) {
            view.setNavigationTitle(event.title)
        }
    }
}