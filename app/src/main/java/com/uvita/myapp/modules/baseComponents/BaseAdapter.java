package com.uvita.myapp.modules.baseComponents;


import androidx.recyclerview.widget.RecyclerView;

public abstract class BaseAdapter<VIEW extends BaseView> extends RecyclerView.Adapter  {
    protected VIEW view;

    public BaseAdapter(VIEW view) {
        this.view = view;
    }

    public BaseAdapter() {
    }

}