package com.uvita.myapp.modules.main

import android.Manifest
import android.app.AlertDialog
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.uvita.myapp.R
import com.uvita.myapp.databinding.ActivityMainBinding
import com.uvita.myapp.general.MyApp
import com.uvita.myapp.models.entities.*
import com.uvita.myapp.modules.baseComponents.BaseActivity
import com.uvita.myapp.modules.repository.local.AppDB

class MainActivity : BaseActivity<MainViewModel, ActivityMainBinding>(), MainView {

    private var frgInspections: Fragment? = null
    private val ROOT_FRAGMENT = "root_fragment"

    override fun getViewModel(): MainViewModel {
        return ViewModelProviders.of(this).get(MainViewModel::class.java)
    }

    override fun initializeDataBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun setNavigationTitle(title: String) {
        supportActionBar?.let {
            it.title = title
        }
    }

    override fun onBackPressed() {
        if (MyApp.blockBack) {
            Toast.makeText(
                applicationContext, "Request in progress...",
                Toast.LENGTH_LONG
            ).show()
        } else {
            val count = supportFragmentManager.backStackEntryCount
            if (count <= 1) {
//                logOutDialog()
            } else {
                supportFragmentManager.popBackStack()
            }
        }
    }
}