package com.uvita.myapp.modules.repository.remote.inspections;

import com.uvita.myapp.modules.repository.remote.BaseRepository;

public class InspectionsRepository extends BaseRepository {
    private static final String TAG = "InspectionsRepository";

    InspectionsInterface getRestInterface() {
        return retrofit.create(InspectionsInterface.class);
    }


//    public void getProjects() {
//        InspectionsInterface inspectionsInterface = getRestInterface();
//        inspectionsInterface.getProjects(getAuthHeaders(), MyApp.currentUser.getClientId(), true).enqueue(new Callback<List<ProjectEntity>>() {
//            @Override
//            public void onResponse(Call<List<ProjectEntity>> call, Response<List<ProjectEntity>> response) {
//                if (response.code() == HTTP_OK) {
//                    if (response.body() != null) {
//
//                        AppDB.getInstance()
//                                .projectsDao()
//                                .insertWithActivities(MyApp.currentUser.getClientId(), response.body());
//                    }
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<ProjectEntity>> call, Throwable t) {
//                // TODO Handle or Log error
//                Log.e(TAG, "Error al recibir los proyectos.", t);
//            }
//        });
//    }
//
//    public void postInspection(final InspectionEntity inspectionEntity) {
//        InspectionsInterface inspectionsInterface = getRestInterface();
//        inspectionsInterface.postInspection(getAuthHeaders(), inspectionEntity).enqueue(new Callback<InspectionEntity>() {
//            @Override
//            public void onResponse(Call<InspectionEntity> call, Response<InspectionEntity> response) {
//                if (response.code() == HTTP_CREATED || response.code() == HTTP_NO_CONTENT) {
//                    if (response.body() != null && response.code() == HTTP_CREATED) {
//                        inspectionEntity.setServerId(response.body().getServerId());
//
//                    }
//                } else {
//                    inspectionEntity.setStatus(InspectionEntity.STATUS_NOT_SYNCHRONIZED);
//                    AppDB.getInstance().inspectionsDao().update(inspectionEntity);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<InspectionEntity> call, Throwable t) {
//                Log.e(TAG, "Error al postear la inspeccion.");
//                inspectionEntity.setStatus(InspectionEntity.STATUS_NOT_SYNCHRONIZED);
//                AppDB.Companion.getInstance().inspectionsDao().update(inspectionEntity);
//            }
//        });
//    }
}
