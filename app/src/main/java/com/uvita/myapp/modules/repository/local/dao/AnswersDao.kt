package com.uvita.myapp.modules.repository.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.uvita.myapp.models.entities.AnswerEntity

@Dao
abstract class AnswersDao : BaseDao<AnswerEntity>() {
    @Query("Select * from answers where questionId=:questionId and sectionId=:sectionId and inspectionId=:inspectionId")
    abstract fun find(questionId: Long, sectionId: Long, inspectionId: Long): AnswerEntity?

    @Query("Select * from answers where id=:answerId")
    abstract fun find(answerId: Long): AnswerEntity?
}