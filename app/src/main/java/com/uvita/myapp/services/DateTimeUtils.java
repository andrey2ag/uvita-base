package com.uvita.myapp.services;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateTimeUtils {
    public final static String PATTERN_SHORT = "dd MMMM yyyy";
    public final static String PATTERN_YYYYMMDD_TIME = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    public final static String PATTERN_DMMYYYY_TIME = "EEE, d MMM yyyy HH:mm:ss Z";
    public final static String PATTERN_MMMD_TIME = "MMM d - hh:mm aa";
    public final static String PATTERN_DMMM_TIME = "d MMM - hh:mm aa";
    public final static String PATTERN_HOUR_SHORT = "hh:mm a";
    public final static String PATTERN_SIMPLE_DATETIME = "yyyyMMdd_HHmmss";



    @StringDef({PATTERN_SHORT, PATTERN_YYYYMMDD_TIME, PATTERN_MMMD_TIME, PATTERN_DMMYYYY_TIME,  PATTERN_HOUR_SHORT, PATTERN_DMMM_TIME})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Patterns {
    }
    public static String getCurrentDatetimeUTC() {
        DateFormat format = new SimpleDateFormat(PATTERN_YYYYMMDD_TIME);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        return format.format(new Date());
    }
    public static String formatDateUTCToLocal(String date, @DateTimeUtils.Patterns String pattern) {
        DateFormat format = new SimpleDateFormat(PATTERN_YYYYMMDD_TIME);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date fDate = null;
        try {
            fDate = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.getDefault());
        return sdf.format(fDate);
    }
    public static String getSimpleDateTime(){
        return new SimpleDateFormat(PATTERN_SIMPLE_DATETIME).format(new Date());

    }
    public static String formatSeconds(int seconds) {
        return getTwoDecimalsValue(seconds / 3600) + ":"
                + getTwoDecimalsValue(seconds / 60) + ":"
                + getTwoDecimalsValue(seconds % 60);
    }

    private static String getTwoDecimalsValue(int value) {
        if (value >= 0 && value <= 9) {
            return "0" + value;
        } else {
            return value + "";
        }
    }

}
