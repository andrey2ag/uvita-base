package com.uvita.myapp.models.entities.pojos

import com.google.gson.annotations.SerializedName

data class InspectionResponse ( @SerializedName("id")
                                var id:Long=0,
                                var name:String="")