package com.uvita.myapp.models.entities

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "answers")
class AnswerEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    @SerializedName("options_selected") var optionId: Long = 0,
    var sectionId: Long = 0,
    @SerializedName("question_id")
    var questionId: Long = 0,
    var inspectionId: Long = 0,
    var free_text: String = ""
//    @Ignore var observations: List<ObservationEntity>? = null
)