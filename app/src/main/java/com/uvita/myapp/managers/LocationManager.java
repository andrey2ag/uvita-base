package com.uvita.myapp.managers;

import android.Manifest;
import android.app.Activity;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;

import android.os.Bundle;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.*;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;


public class LocationManager implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private Activity activity;
    private LocationReceiver locationReceiver;
    private FusedLocationProviderClient mFusedLocationClient;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private LocationCallback mLocationCallback;
    private int currentLocationReceiverType;
    private Location location = null;
    public final static int LOCATION_RECEIVER_VIDEO = 9002;
    public final static int LOCATION_RECEIVER_TRACKING_INSPECTION = 9003;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static int LOCATION_REQUEST_INTERVAL = 10000; // milliseconds
    private static int LOCATION_REQUEST_FASTEST_INTERVAL = 1000; //milliseconds
    private static int LOCATION_REQUEST_SMALLEST_DISPLACEMENT = 10; //meters

    private static LocationManager instance;

    public LocationManager(Activity activity, LocationReceiver locationReceiver, int locationReceiverType, int locationRequestInterval,int locationRequesFastestInterval,int locationRequestSmallestDisplacement) {

        setActivity(activity);
        currentLocationReceiverType = locationReceiverType;
        setLocationReceiver(locationReceiver);
        LOCATION_REQUEST_INTERVAL = locationRequestInterval;
        LOCATION_REQUEST_FASTEST_INTERVAL = locationRequesFastestInterval;
        LOCATION_REQUEST_SMALLEST_DISPLACEMENT = locationRequestSmallestDisplacement;
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this.activity);
        initClient();
    }

    private void setActivity(Activity activity) {
        this.activity = activity;
    }

    private void setLocationReceiver(LocationReceiver locationReceiver) {
        this.locationReceiver = locationReceiver;
    }
    private void setLocationReceiverType(int locationReceiverType) {
        this.currentLocationReceiverType = locationReceiverType;
    }


    public static LocationManager getInstance(Activity activity, LocationReceiver locationReceiver, int locationReceiverType){
        if(instance==null){
            instance=new LocationManager(activity, locationReceiver, locationReceiverType);
        }else{
            instance.setActivity(activity);
            instance.setLocationReceiver(locationReceiver);
            instance.setLocationReceiverType(locationReceiverType);
        }

        return instance;
    }

    private LocationManager(Activity activity, LocationReceiver locationReceiver, int locationReceiverType) {
        this.activity = activity;
        this.locationReceiver = locationReceiver;
        this.currentLocationReceiverType = locationReceiverType;
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this.activity);
        initClient();
    }

    public interface LocationReceiver {
        void onLocationChanged(@NonNull Location location, int locationReceiverType);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            locationReceiver.onLocationChanged(location, currentLocationReceiverType);
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(activity, location -> {
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {
                        // Logic to handle location object
                        locationReceiver.onLocationChanged(location, currentLocationReceiverType);
                    }
                });

       /* Location location = FusedLocationApi
                .getLastLocation(googleApiClient);

        if (location != null) {
            locationReceiver.onLocationChanged(location);
        }*/

       /* FusedLocationApi
                .requestLocationUpdates(googleApiClient,
                        locationRequest,  this);*/
        mFusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper());
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(activity,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i(
                    LocationManager.class.getSimpleName(),
                    "Location services connection failed with code " + connectionResult.getErrorCode()
            );
        }
    }
    private void initClient() {
        googleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(LOCATION_REQUEST_INTERVAL)
                .setFastestInterval(LOCATION_REQUEST_FASTEST_INTERVAL)
                .setSmallestDisplacement(LOCATION_REQUEST_SMALLEST_DISPLACEMENT);

        mLocationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                for (Location location : locationResult.getLocations()) {
                    onLocationChanged(location);

                }
            }

        };
    }
    public void connect() {
        if (!isConnected()) {
            Dexter.withActivity(activity)
                    .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                    .withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            googleApiClient.connect();
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                        }
                    }).check();
        }
    }

    public void disconnect() {
        if (googleApiClient.isConnected()) {
            // DEV NOTE: Keep removeLocationUpdate before disconnect() sentence, to avoid crash
            // because of unavailable googleApiClient

            mFusedLocationClient.removeLocationUpdates(mLocationCallback);

           /* FusedLocationApi
                    .removeLocationUpdates(googleApiClient,  this);*/
            googleApiClient.disconnect();
        }
    }

    private boolean isConnected() {
        return googleApiClient.isConnected();
    }
    public @Nullable Location getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */

        if(!googleApiClient.isConnected()){
            connect();
        }
        if (googleApiClient.isConnected()) {
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                return new Location("dummyprovider");
            }
            /*location = FusedLocationApi
                    .getLastLocation(googleApiClient);*/
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(activity, (Location lastLocation) -> {
                        // Got last known location. In some rare situations this can be null.
                        if (lastLocation != null) {
                            // Logic to handle location object
                            location=lastLocation;
                        }
                    });

        }
        if (location==null){
            location = new Location("dummyprovider");
        }
        return location;

    }
}
